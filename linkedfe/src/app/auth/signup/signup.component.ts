import { Component, OnInit, ViewChild } from '@angular/core';
import {NgForm} from '@angular/forms';
import { user } from '../../users/user.model';
import { SignupService } from './signup.service';
import {throwError} from 'rxjs';
import { UsersService } from 'src/app/users/users.service';
@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  @ViewChild('f') userForm: NgForm;
  user:{username:String,password:String}
  constructor(private suService:SignupService,private userservice:UsersService) { }

  ngOnInit() {
    this.user={
      username:'',
      password:'',
    }
  }
  login() {
    this.suService.login({'username': this.user.username, 'password': this.user.password});
  }
 
  refreshToken() {
    this.suService.refreshToken();
  }
 
  logout() {
    this.suService.logout();
  }

  onSubmit(){
    
    this.userservice.addUser(
      this.userForm.value.id,this.userForm.value.empleadooempleador,this.userForm.value.name,1,this.userForm.value.password)
    .subscribe(
      (response)=> console.log(response),
      (error)=> console.log(error)
    )
    
}

}
