import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';

import { SignupService } from '../signup/signup.service';
import {throwError} from 'rxjs';
@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {

  user:{username:String,password:String}
  constructor(private suService:SignupService) { }

  ngOnInit() {
    this.user={
      username:'',
      password:'',
    }
  }
  login() {
    this.suService.login({'username': this.user.username, 'password': this.user.password});
  }
 
  refreshToken() {
    this.suService.refreshToken();
  }
 
  logout() {
    this.suService.logout();
  }

  onSignup(form:NgForm){
    const username= form.value.username;
    const password= form.value.password;
  }


}
