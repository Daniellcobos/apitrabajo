import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';

import { HttpModule} from '@angular/http'


import { AppComponent } from './app.component';
import { EmpleadoComponent } from './empleado/empleado.component';
import { HomeComponent } from './home/home.component';

import { EmpleadoService } from './empleado/empleado.service';
import { IndempleadoComponent } from './indempleado/indempleado.component'
import { FormsModule } from '@angular/forms';
import { UsersComponent } from './users/users.component';
import { InduserComponent } from './users/induser/induser.component';
import { UsersService } from './users/users.service';
import { CategoriasComponent } from './categorias/categorias.component';
import { CatindComponent } from './categorias/catind/catind.component';
import { TrabajosComponent } from './categorias/trabajos/trabajos.component';
import { SignupComponent } from './auth/signup/signup.component';
import { SigninComponent } from './auth/signin/signin.component';
import { AuthGuardService } from './auth-guard.service';

const routes: Routes = [{path:'' , component: HomeComponent},
{path:'empleados' , component: EmpleadoComponent,canActivate:[AuthGuardService] ,children: [
{path:':user' , component: IndempleadoComponent}]
}, 
{path:'usuarios' , component: UsersComponent ,children: [
{path:':username' , component: InduserComponent}]
},
{path:'categorias' , component: CategoriasComponent ,children: [
{path:':id' , component: CatindComponent},]},
{path:'trabajos/:id',component:TrabajosComponent},
{path:'signup',component:SignupComponent},
{path:'signin',component:SigninComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
