import { Injectable } from '@angular/core';
import { Http,Response } from '@angular/http';
import { Subject } from 'rxjs';
import { Categoria } from './categoria.model';

@Injectable({
  providedIn: 'root'
})
export class CategoriaService {
  categoriasChanged= new Subject<Categoria[]>();
  constructor(private http:Http) { }
  private categorias: Categoria[]=[]

  setCategorias(categorias: Categoria[]){
    this.categorias=categorias
    this.categoriasChanged.next(this.categorias.slice());
  }

  getCategorias(){
    this.http.get('http://localhost:8000/trabajos/api/cat')
    .subscribe(
      (response:Response)=>{
        const catserver: Categoria[] = response.json();
        this.setCategorias(catserver);
        console.log(this.categorias.slice())
      }
    )
    return this.categorias.slice();
  }
    
}
