import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CatindComponent } from './catind.component';

describe('CatindComponent', () => {
  let component: CatindComponent;
  let fixture: ComponentFixture<CatindComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CatindComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CatindComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
