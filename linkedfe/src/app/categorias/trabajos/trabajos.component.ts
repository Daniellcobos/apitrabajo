import { Component, OnInit } from '@angular/core';
import { Trabajo } from './trabajo.model';
import { ActivatedRoute, Params} from '@angular/router';
import { TrabajoService } from './trabajo.service';

@Component({
  selector: 'app-trabajos',
  templateUrl: './trabajos.component.html',
  styleUrls: ['./trabajos.component.css']
})
export class TrabajosComponent implements OnInit {
  trabajo=this.tService.getThisTrabajo(1)
  strabajo=this.tService.getThisTrabajo(1)
  
  constructor(private route:ActivatedRoute, private tService:TrabajoService) { }

  
  
  ngOnInit() {
    
    this.route.params
    .subscribe(
     (params:Params)=>{
       const url=params['id'];
       this.tService.getThisTrabajo(url).subscribe(
         (data:any)=>{
           console.log(data)
           this.trabajo=data;
         }
       )
       
     }
    );
  }
  }


