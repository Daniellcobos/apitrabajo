export class Trabajo{
    id: number
    nombre: string
    campo_empleo: number
    salario: number
    intensidad_horaria: number
    
    constructor(id:number,nam:string,camp:number,salario:number,inth:number){
      this.id=id
      this.nombre=nam
      this.campo_empleo=camp
      this.salario=salario
      this.intensidad_horaria=inth

    }

}