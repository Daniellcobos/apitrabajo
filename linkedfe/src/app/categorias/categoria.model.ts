import { Trabajo } from "./trabajos/trabajo.model";

export class Categoria {
    id:number;
    nombre_cat:string;
    trabajos:Trabajo[]

    constructor(nid:number,nc:string){
     this.id=nid;
     this.nombre_cat=nc;
     this.trabajos=[]
    }
}