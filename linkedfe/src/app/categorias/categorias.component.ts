import { Component, OnInit } from '@angular/core';
import { CategoriaService } from './categoria.service';
import { Categoria } from './categoria.model';

@Component({
  selector: 'app-categorias',
  templateUrl: './categorias.component.html',
  styleUrls: ['./categorias.component.css']
})
export class CategoriasComponent implements OnInit {
  private categorias: Categoria[]=[];
  constructor(private cservice:CategoriaService) {}

  ngOnInit() {
    this.categorias=this.cservice.getCategorias();
  }

}
