import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule} from '@angular/http'

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { EmpleadoComponent } from './empleado/empleado.component';
import { HomeComponent } from './home/home.component';
import { Routes,RouterModule } from '@angular/router';
import { EmpleadoService } from './empleado/empleado.service';
import { IndempleadoComponent } from './indempleado/indempleado.component'
import { FormsModule } from '@angular/forms';
import { UsersComponent } from './users/users.component';
import { InduserComponent } from './users/induser/induser.component';
import { UsersService } from './users/users.service';
import { CategoriasComponent } from './categorias/categorias.component';
import { CatindComponent } from './categorias/catind/catind.component';
import { TrabajosComponent } from './categorias/trabajos/trabajos.component';
import { SignupComponent } from './auth/signup/signup.component';
import { SigninComponent } from './auth/signin/signin.component';
import { HttpClientModule } from '@angular/common/http';
import { AuthGuardService } from './auth-guard.service';
import { SignupService } from './auth/signup/signup.service';


const appRoutes: Routes = [
  
];




@NgModule({
  declarations: [
    AppComponent,
    EmpleadoComponent,
    HomeComponent,
    IndempleadoComponent,
    UsersComponent,
    InduserComponent,
    CategoriasComponent,
    CatindComponent,
    TrabajosComponent,
    SignupComponent,
    SigninComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot(appRoutes),
    HttpModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [EmpleadoService,UsersService,AuthGuardService,SignupService],
  bootstrap: [AppComponent]
})
export class AppModule { }
