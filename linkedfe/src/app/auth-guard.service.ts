import { Injectable } from '@angular/core';

import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router,
  CanActivateChild
} from '@angular/router';
import { Observable } from 'rxjs';
import { SignupService } from './auth/signup/signup.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate{
  constructor(private suservice:SignupService, private router:Router){}
  canActivate(route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
return this.suservice.islogged()
.then(
(authenticated: boolean) => {
if (authenticated) {
  return true;
} else {
  this.router.navigate(['/']);
}
}
);
}
}