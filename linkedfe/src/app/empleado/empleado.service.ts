import { Injectable } from '@angular/core';
import { Empleado } from './empleado.model';
import 'rxjs/Rx'
import { Http,Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { fromEventPattern } from 'rxjs';
import { SignupService } from '../auth/signup/signup.service';
import { HttpClient,HttpHeaders } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})

export class EmpleadoService {
empleadosChanged = new Subject<Empleado[]>();
constructor(private http:Http,private suservice:SignupService, private htttp:HttpClient){}
private empleados : Empleado[] = []
private indempleado: Empleado = {user: 0, estado_empleo: false};
setEmpleados(empleados:Empleado[]){
  this.empleados=empleados
  this.empleadosChanged.next(this.empleados.slice());
}
getDummyEmpleados(){
  return this.empleados
}

getEmpleados(){
  return this.http.get('http://localhost:8000/trabajos/api/empleados')
  .map(
    (response:Response)=>{
      const empleadosserver = response.json();
       console.log(empleadosserver);
       return empleadosserver;
       
    }
  );
 
}
getThisEmpleado(user:number){
  const urluser=String(user);
  this.http.get('http://localhost:8000/trabajos/api/empleados/' + urluser) 
  .subscribe(
    (response:Response)=>{
      const empleadosserver: Empleado = response.json();
       this.indempleado= empleadosserver;
       console.log(this.indempleado);
       
    }
  );
 return this.indempleado;
}
addEmpleado(usern:number,ee:boolean){
  const httpOptions = {
      
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'JWT '+this.suservice.token    // this is our token from the UserService (see Part 1)
    })
  };
  (
    this.indempleado={
      user:usern,
      estado_empleo:ee
    }
  )
  return this.htttp.post('http://localhost:8000/trabajos/api/empleados',this.indempleado,httpOptions);
}
deleteEmpleado(user:number){
  const httpOptions = {
      
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'JWT '+this.suservice.token    // this is our token from the UserService (see Part 1)
    })
  };
  const urluser=String(user);
  this.htttp.delete('http://localhost:8000/trabajos/api/empleados/' + urluser,httpOptions) 
  .subscribe(
    (response:Response)=>{
      
       console.log('rip ' + user);
       
    }
  );

}

}
