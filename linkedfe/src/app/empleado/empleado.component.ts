import { Component, OnInit, ViewChild} from '@angular/core';
import { Empleado } from './empleado.model';
import { EmpleadoService } from './empleado.service';
import { NgForm } from '@angular/forms';
@Component({
  selector: 'app-empleado',
  templateUrl: './empleado.component.html',
  styleUrls: ['./empleado.component.css']
})
export class EmpleadoComponent implements OnInit {
  @ViewChild('f') empleadoForm: NgForm;
  private trueempleados: Empleado[]= [];
  constructor(private empleadoService:EmpleadoService) { }
  estadotemp= true;
  ngOnInit() {

    this.empleadoService.getEmpleados()
    .subscribe(
    (data:any)=>{
      this.trueempleados=data;
    }
    );
  }
  onLoadEmpleados(){
    this.empleadoService.getEmpleados();
  }
  onSubmit(){
    this.empleadoService.addEmpleado(this.empleadoForm.value.id,this.empleadoForm.value.estadoempleo)
    .subscribe(
      (response)=> console.log(response),
      (error)=> console.log(error)
    )
    
    
  }
}
