import { Injectable } from '@angular/core';
import { Http,Response } from '@angular/http';
import { Subject } from 'rxjs';
import { user } from './user.model';
import { SignupService } from '../auth/signup/signup.service';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import 'rxjs/Rx'
@Injectable({
  providedIn: 'root'
})
export class UsersService {
  usersChanged=new Subject<user[]>();
  constructor(private http:Http,private htttp:HttpClient,private suservice:SignupService){}
  private usuarios : user[]=[];
  private usuario: user={id:0,empleadooempleador:false,campo_empleo:1,username:'',password:''};
  
  setUsuarios(usuarios:user[]){
    this.usuarios=usuarios
    this.usersChanged.next(this.usuarios.slice());
  }
  
  getUsuarios(){
    return this.http.get('http://localhost:8000/trabajos/api/usuarios')
    .map(
      (response:Response)=>{
        const usuariosserver = response.json();
         
         console.log(usuariosserver);
         return usuariosserver;
         
      }
    );
   
  }
 
  getThisUser(username:string){
    
     return this.http.get('http://localhost:8000/trabajos/api/usuarios/' + username) 
    .map(
      (response:Response)=>{
        const userserver = response.json();
        console.log(userserver);
        return userserver;
         
         
      }
    );
  
  }
 
  addUser(usern:number,ee:boolean,nombre:string,ce:number,pass:string){
   
      this.usuario={
        id:usern,
        empleadooempleador:ee,
        username:nombre,
        campo_empleo:ce,
        password:pass
      }
    
    const httpOptions = {
      
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'JWT '+this.suservice.token    // this is our token from the UserService (see Part 1)
      })
    };
    return this.htttp.post('http://localhost:8000/trabajos/api/usuarios',this.usuario,httpOptions);
  }
  
  deleteUser(user:number){
    const urluser=String(user);
    this.http.delete('http://localhost:8000/trabajos/api/usuarios/' + urluser) 
    .subscribe(
      (response:Response)=>{
        
         console.log(response + 'rip in peperonni');
         
      }
    );
 
}
}
