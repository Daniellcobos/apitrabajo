import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InduserComponent } from './induser.component';

describe('InduserComponent', () => {
  let component: InduserComponent;
  let fixture: ComponentFixture<InduserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InduserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InduserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
