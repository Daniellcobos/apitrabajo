import { Component, OnInit,OnChanges } from '@angular/core';
import { ActivatedRoute, Params} from '@angular/router';
import { UsersService} from '../users.service';
import { user } from '../user.model';
@Component({
  selector: 'app-induser',
  templateUrl: './induser.component.html',
  styleUrls: ['./induser.component.css']
})
export class InduserComponent implements OnInit {
  usuario: user={id:0,empleadooempleador:false,campo_empleo:1,username:'',password:''};
  constructor(private route:ActivatedRoute, private uservice:UsersService) { }

  ngOnInit() {
    
    this.route.params
    .subscribe(
     (params:Params)=>{
       const url=String(params['username']);
       console.log(url)
       this.uservice.getThisUser(url)
       .subscribe(
         (data:any)=>{
           this.usuario=data;
         }
       )
     }
    );
  }
 onDelete(){
  const url=this.route.snapshot.params['username'];
  this.uservice.deleteUser(url);
 }
  
  
  
  
  
  }


