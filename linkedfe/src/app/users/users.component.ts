import { Component, OnInit} from '@angular/core';


import { user } from './user.model';
import { UsersService } from './users.service';
import { SignupService } from '../auth/signup/signup.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  
  private usuarios:user[]=[];
  estadotemp= true;
  constructor(private userservice:UsersService, private suservice:SignupService) { }
  ngOnInit() {
    this.userservice.getUsuarios()
    .subscribe(
      (data:any)=>{
        this.usuarios=data;
      }
    )
  }

}