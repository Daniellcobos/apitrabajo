import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IndempleadoComponent } from './indempleado.component';

describe('IndempleadoComponent', () => {
  let component: IndempleadoComponent;
  let fixture: ComponentFixture<IndempleadoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IndempleadoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IndempleadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
