import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params} from '@angular/router';
import { EmpleadoService} from '../empleado/empleado.service';
import { Empleado } from '../empleado/empleado.model';
import { from } from 'rxjs';
@Component({
  selector: 'app-indempleado',
  templateUrl: './indempleado.component.html',
  styleUrls: ['./indempleado.component.css']
})
export class IndempleadoComponent implements OnInit  {
  empleado: Empleado = {user:0,estado_empleo:false} ; 
  constructor(private route:ActivatedRoute,private indservice:EmpleadoService) { }
  
  ngOnInit() {
    const url=this.route.snapshot.params['user'];
    this.empleado=this.indservice.getThisEmpleado(url);
    this.route.params
    .subscribe(
     (params:Params)=>{
       const url=params['user'];
       this.empleado=this.indservice.getThisEmpleado(url);
     }
    );
  }
 onDelete(){
  const url=this.route.snapshot.params['user'];
  this.indservice.deleteEmpleado(url);
 }
}
