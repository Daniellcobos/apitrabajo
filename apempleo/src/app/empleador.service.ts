import { Injectable } from '@angular/core';
import { empleador } from './empleador';
import { EMPLEADORES } from './mockheroes';
import { Observable, of } from 'rxjs';
import { MessageService } from './message.service';
@Injectable({
  providedIn: 'root'
})
export class EmpleadorService {
  getEmpleadores(): Observable<empleador[]> {
  this.messageService.add('EmpleadorService: empleadores obtenidos');
  return of(EMPLEADORES);
}
  constructor(private messageService: MessageService) { }
  getEmpleador(id: number): Observable<empleador> {
  // TODO: send the message _after_ fetching the empleador
  this.messageService.add(`EmpleadorService: fetched empleador id=${id}`);
  return of(EMPLEADORES.find(empleador => empleador.id === id));
}
}
