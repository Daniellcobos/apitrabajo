import { Component, OnInit } from '@angular/core';
import { empleador } from '../empleador';
import { EmpleadorService } from '../empleador.service';
import { TrabajoService } from '../trabajo.service';
import { trabajo } from '../trabajo';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: [ './dashboard.component.css' ]
})
export class DashboardComponent implements OnInit {
  empleadores: empleador[] = [];
  trabajos: trabajo[] = [];
  constructor(private empleadorService: EmpleadorService,private trabajoService: TrabajoService) { }

  ngOnInit() {
    this.getEmpleadores();
    this.getTrabajos();
  }

  getEmpleadores(): void {
    this.empleadorService.getEmpleadores()
      .subscribe(empleadores => this.empleadores = empleadores.slice(1, 5));
  }
  getTrabajos(): void {
    this.trabajoService.getTrabajos()
      .subscribe(trabajos => this.trabajos = trabajos.slice(1, 5));
  }
}
