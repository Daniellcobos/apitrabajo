import { TestBed, inject } from '@angular/core/testing';

import { EmpleadorService } from './empleador.service';

describe('EmpleadorService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EmpleadorService]
    });
  });

  it('should be created', inject([EmpleadorService], (service: EmpleadorService) => {
    expect(service).toBeTruthy();
  }));
});
