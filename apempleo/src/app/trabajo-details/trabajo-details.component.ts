import { Component, OnInit, Input  } from '@angular/core';
import { trabajo } from '../trabajo';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { TrabajoService }  from '../trabajo.service';

@Component({
  selector: 'app-trabajo-details',
  templateUrl: './trabajo-details.component.html',
  styleUrls: ['./trabajo-details.component.css']
})
export class TrabajoDetailsComponent implements OnInit {
  @Input() trabajo: trabajo;
  constructor(
    private route: ActivatedRoute,
    private trabajoService: TrabajoService,
    private location: Location,)
  {}

  ngOnInit(): void {
    this.getTrabajo();
  }

  getTrabajo(): void {
  const id = +this.route.snapshot.paramMap.get('id');
  this.trabajoService.getTrabajo(id)
    .subscribe(trabajo => this.trabajo = trabajo);
   }

  goBack(): void {
  this.location.back();
   }

}
