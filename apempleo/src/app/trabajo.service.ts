import { Injectable } from '@angular/core';
import { trabajo } from './trabajo';
import { TRABAJOS } from './mocktrabajadores';
import { Observable, of } from 'rxjs';
import { MessageService } from './message.service';
@Injectable({
  providedIn: 'root'
})
export class TrabajoService {
  getTrabajos(): Observable<trabajo[]> {
  this.messageService.add('TrabajoService: trabajos obtenidos');
  return of(TRABAJOS);
}
  constructor(private messageService: MessageService) { }
  getTrabajo(id: number): Observable<trabajo> {
  // TODO: send the message _after_ fetching the trabajo
  this.messageService.add(`TrabajoService: fetched trabajo id=${id}`);
  return of(TRABAJOS.find(trabajo => trabajo.id === id));
}
}
