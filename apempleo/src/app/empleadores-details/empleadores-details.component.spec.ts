import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmpleadoresDetailsComponent } from './empleadores-details.component';

describe('EmpleadoresDetailsComponent', () => {
  let component: EmpleadoresDetailsComponent;
  let fixture: ComponentFixture<EmpleadoresDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmpleadoresDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmpleadoresDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
