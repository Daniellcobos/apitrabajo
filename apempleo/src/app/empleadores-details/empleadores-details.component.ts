import { Component, OnInit, Input } from '@angular/core';
import { empleador } from '../empleador';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { EmpleadorService }  from '../empleador.service';

@Component({
  selector: 'app-empleador-detail',
  templateUrl: './empleadores-details.component.html',
  styleUrls: ['./empleadores-details.component.css']
})
export class EmpleadoresDetailsComponent implements OnInit {
  @Input() empleador: empleador;

  constructor(
    private route: ActivatedRoute,
    private empleadorService: EmpleadorService,
    private location: Location,)
  {}

  ngOnInit(): void {
    this.getEmpleador();
  }

  getEmpleador(): void {
  const id = +this.route.snapshot.paramMap.get('id');
  this.empleadorService.getEmpleador(id)
    .subscribe(empleador => this.empleador = empleador);
   }

  goBack(): void {
  this.location.back();
   }
}
