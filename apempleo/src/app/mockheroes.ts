import { empleador } from './empleador';

export const EMPLEADORES: empleador[] = [
  { id: 11, name: 'Mr. Nice', reputacion: 5,ofertas: 'lavaperros'},
  { id: 12, name: 'Natyrcoa', reputacion: 5,ofertas: 'lavaperros' },
  { id: 13, name: 'Bombasto' , reputacion: 5,ofertas: 'lavaperros'},
  { id: 14, name: 'Celeritas' , reputacion: 5,ofertas: 'lavaperros'},
  { id: 15, name: 'Magneta' , reputacion: 5,ofertas: 'lavaperros'},
  { id: 16, name: 'RubberMan' , reputacion: 5,ofertas: 'lavaperros'},
  { id: 17, name: 'Dynama', reputacion: 5,ofertas: 'lavaperros' },
  { id: 18, name: 'Dr IQ' , reputacion: 5,ofertas: 'lavaperros'},
  { id: 19, name: 'Magma' , reputacion: 5,ofertas: 'lavaperros'},
  { id: 20, name: 'Tornado', reputacion: 5,ofertas: 'lavaperros' }
]
