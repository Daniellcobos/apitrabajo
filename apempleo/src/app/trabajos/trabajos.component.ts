import { Component, OnInit } from '@angular/core';
import { trabajo } from '../trabajo';
import { TrabajoService } from '../trabajo.service';
@Component({
  selector: 'app-trabajos',
  templateUrl: './trabajos.component.html',
  styleUrls: ['./trabajos.component.css']
})
export class TrabajosComponent implements OnInit {


    trabajos: trabajo[];

    selectedtrabajo: trabajo;


    constructor(private trabajoService: TrabajoService) { }

    ngOnInit() {
      this.getTrabajos();
    }


    getTrabajos(): void {
    this.trabajoService.getTrabajos()
     .subscribe(trabajos => this.trabajos = trabajos);
    }
}
