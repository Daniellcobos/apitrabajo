import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmpleadoresComponent} from './empleadores/empleadores.component'
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent }   from './dashboard/dashboard.component';
import { EmpleadoresDetailsComponent }  from './empleadores-details/empleadores-details.component';
import { TrabajoDetailsComponent }  from './trabajo-details/trabajo-details.component';
import { TrabajosComponent} from './trabajos/trabajos.component'
const routes: Routes = [
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  { path: 'empleadores', component: EmpleadoresComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'detail/:id', component: EmpleadoresDetailsComponent },
  { path: 'detailt/:id', component: TrabajoDetailsComponent },
  { path: 'trabajos', component: TrabajosComponent },

];




@NgModule({
  imports: [ RouterModule.forRoot(routes),CommonModule ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
