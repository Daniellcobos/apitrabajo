import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms'; // <-- NgModel lives here
import { AppComponent } from './app.component';
import { TrabajosComponent } from './trabajos/trabajos.component';
import { TrabajoDetailsComponent } from './trabajo-details/trabajo-details.component';
import { EmpleadoresComponent } from './empleadores/empleadores.component';
import { EmpleadoresDetailsComponent } from './empleadores-details/empleadores-details.component';
import { MessagesComponent } from './messages/messages.component';
import { AppRoutingModule } from './/app-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';

@NgModule({
  declarations: [
    AppComponent,
    TrabajosComponent,
    TrabajoDetailsComponent,
    EmpleadoresComponent,
    EmpleadoresDetailsComponent,
    MessagesComponent,
    DashboardComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
