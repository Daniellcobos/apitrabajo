import { Component, OnInit } from '@angular/core';
import { empleador } from '../empleador';
import { EmpleadorService } from '../empleador.service';

@Component({
  selector: 'app-empleadores',
  templateUrl: './empleadores.component.html',
  styleUrls: ['./empleadores.component.css']
})
export class EmpleadoresComponent implements OnInit {

  empleadores: empleador[];

  selectedempleador: empleador;


  constructor(private empleadorService: EmpleadorService) { }

  ngOnInit() {
    this.getEmpleadores();
  }


  getEmpleadores(): void {
  this.empleadorService.getEmpleadores()
   .subscribe(empleadores => this.empleadores = empleadores);
  }
}
